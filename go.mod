module EVEServerTicker

go 1.13

require (
	github.com/beevik/ntp v0.3.0
	github.com/mum4k/termdash v0.12.1
	github.com/nsf/termbox-go v0.0.0-20200418040025-38ba6e5628f1 // indirect
	github.com/sparrc/go-ping v0.0.0-20190613174326-4e5b6552494c
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
)
