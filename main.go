package main

import (
	"EVEServerTicker/handlers"
	"context"
	"fmt"
	"strings"
	"time"
	// ui stuff
	"github.com/mum4k/termdash"
	"github.com/mum4k/termdash/cell"
	"github.com/mum4k/termdash/container"
	"github.com/mum4k/termdash/linestyle"
	"github.com/mum4k/termdash/terminal/termbox"
	"github.com/mum4k/termdash/terminal/terminalapi"
	"github.com/mum4k/termdash/widgets/button"
	"github.com/mum4k/termdash/widgets/segmentdisplay"
)

func (i *Instance) clock(ctx context.Context, sd *segmentdisplay.SegmentDisplay) {
	// setup ticker and blocker
	ticker := time.NewTicker(5*time.Millisecond)
	defer ticker.Stop()
	blocked := false

	for {
		select {
		case <-ticker.C:

			// some stuff to update latency and block if it's already been updated this second
			// twice a minute automatically seems reasonable, still providing manual way as well
			if time.Now().Second() == 30|00 && blocked != true {
				blocked = false
			}
			if time.Now().Second() == 29|59 && blocked != true {
				blocked = true
				i.rtt = handlers.Tqping()
			}

			// do the time adjustment
			now := time.Now().Add(i.rtt)
			nowStr := now.Format("15 04 05")
			parts := strings.Split(nowStr, " ")

			// spacers around the second counter
			spacer := " "
			if now.Second()%2 == 0 {
				spacer = ":"
			}

			color := cell.ColorRed
			if now.Second()%2 == 0 {
				color = cell.ColorYellow
			}

			chunks := []*segmentdisplay.TextChunk{
				segmentdisplay.NewChunk(spacer, segmentdisplay.WriteCellOpts(cell.BgColor(color))),
				segmentdisplay.NewChunk(spacer, segmentdisplay.WriteCellOpts(cell.BgColor(color))),
				segmentdisplay.NewChunk(parts[2], segmentdisplay.WriteCellOpts(cell.BgColor(color))),
				segmentdisplay.NewChunk(spacer, segmentdisplay.WriteCellOpts(cell.BgColor(color))),
				segmentdisplay.NewChunk(spacer, segmentdisplay.WriteCellOpts(cell.BgColor(color))),
			}
			if err := sd.Write(chunks); err != nil {
				panic(err)
			}

		case <-ctx.Done():
			return
		}
	}
}


func main() {
	// get ping on startup
	instance := new(Instance)
	instance.rtt = handlers.Tqping()

	t, err := termbox.New()
	if err != nil {
		panic(err)
	}
	defer t.Close()

	ctx, cancel := context.WithCancel(context.Background())
	clockSD, err := segmentdisplay.New()
	if err != nil {
		panic(err)
	}
	go instance.clock(ctx, clockSD)


	display, err := segmentdisplay.New()
	if err != nil {
		panic(err)
	}
	if err := display.Write([]*segmentdisplay.TextChunk{
		segmentdisplay.NewChunk(fmt.Sprintf("%s", instance.rtt)),
	}); err != nil {
		panic(err)
	}

	// tick handler
	tqLatencyButton, err := button.New("Re-Sync with TQ Latency (30s automatic)", func() error {
		instance.rtt = handlers.Tqping()
		return display.Write([]*segmentdisplay.TextChunk{
			segmentdisplay.NewChunk(fmt.Sprintf("%s", instance.rtt)),
		})
	},
		button.FillColor(cell.ColorNumber(220)),
		button.GlobalKey('u'),
	)
	if err != nil {
		panic(err)
	}

	// actual ui here
	c, err := container.New(
		t,
		container.ID("mainUI"),
		container.Border(linestyle.Light),
		container.BorderTitle("PRESS \"Q\" TO QUIT | PRESS \"U\" TO RE-SYNC"),
		container.SplitHorizontal(
			container.Top(
				container.PlaceWidget(tqLatencyButton),
			),
			container.Bottom(
				container.PlaceWidget(clockSD),
			),
			container.SplitPercent(60),
		),
	)
	if err != nil {
		panic(err)
	}


	// handle exit
	quitter := func(k *terminalapi.Keyboard) {
		if k.Key == 'q' || k.Key == 'Q' {
			cancel()
		}
	}

	// startup UI
	if err := termdash.Run(ctx, t, c, termdash.KeyboardSubscriber(quitter), termdash.RedrawInterval(10 * time.Millisecond)); err != nil {
		panic(err)
	}
}
