package handlers

import (
	"github.com/sparrc/go-ping"
	"time"
)

func Tqping() time.Duration {
	pinger, err := ping.NewPinger("87.237.34.200") // srv200 on tq
	pinger.SetPrivileged(true)
	if err != nil {
		panic(err)
	}
	pinger.Count = 10
	pinger.Interval = 100
	pinger.Run() // blocks until finished
	stats := pinger.Statistics() // get send/receive/rtt stats
	//fmt.Printf("avg: %s\n", stats.AvgRtt)
	return stats.AvgRtt
}
